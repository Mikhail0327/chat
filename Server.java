package server;

/**
 * Created by Misha on 26.02.2017.
 */

import java.io.*;
import java.net.*;

class Server extends Thread {
    Socket s;
    int num;

    public static void main(String args[]) {
        try {
            int i = 0; // счетчик подключений

            // добавить сокет на порт 4239
            ServerSocket server = new ServerSocket(4239, 0,
                    InetAddress.getByName("localhost"));

            System.out.println("server is started");

            while(true)
            {
                /*
                ожидать нового подключения, затем
                запустить обработку клиента в новый
                вычислительный поток и увеличить счетчик на 1
                */
                new Server(i, server.accept());
                i++;
            }
        }
        catch(Exception e)
        {System.out.println("init error: "+e);} // вывод исключений
    }

    public Server(int num, Socket s) {
        // копировать данные
        this.num = num;
        this.s = s;

        // запустить новый вычислительный поток
        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }

    public void run() {
        try {
            // получить из сокета клиента поток входящих данных
            InputStream is = s.getInputStream();
            // получить из сокета клиента поток данных от сервера к клиенту
            OutputStream os = s.getOutputStream();

            // буффер данных в 64 килобайта
            byte buf[] = new byte[64*1024];
            // читать 64кб от клиента, результат —
            // кол-во реально принятых данных
            int r = is.read(buf);

            // создать строку, содержащую полученную от клиента информацию
            String data = new String(buf, 0, r);

            // добавляем данные об адресе сокета:
            data = ""+num+": "+"\n"+data;

            // вывести данные:
            os.write(data.getBytes());

            // завершить соединение
            s.close();
        }
        catch(Exception e)
        {System.out.println("init error: "+e);} // вывод исключений
    }
}